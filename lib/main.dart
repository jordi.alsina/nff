import 'dart:js';
import 'package:flutter/material.dart';
import 'package:english_words/english_words.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Messages',
      home: Scaffold(
        body: Container(
          child: Row(children: <Widget>[
            Icon(Icons.arrow_back_outlined),
            Text(
              "Nigeria Football Federation",
              style: TextStyle(
                fontSize: 20.0,
                fontWeight: FontWeight.w500,
                fontFamily: 'Roboto',
                color: Colors.white,
              ),
            )
          ]),
          width: 377.0,
          height: 247.0,
          margin: const EdgeInsets.only(left: 0, top: 0),
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/team_nff.png"),
              fit: BoxFit.cover,
            ),
          ),
        ),
      ),
    );
  }
}

/*
 margin: const EdgeInsets.only(left: 0.0, top: 0.0),

Icon(Icons.arrow_back_outlined),

Text(
"Nigeria Football Federation",
style: TextStyle(
fontSize: 20.0,
fontWeight: FontWeight.w500,
fontFamily: 'Roboto'),
textAlign: TextAlign.center,
)

Image(image: AssetImage("assets/images/team_nff.png")),
*/
